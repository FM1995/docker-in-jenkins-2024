# Docker in Jenkins 2024

### Pre-Requisites
Digital Ocean Droplet

Docker

Jenkins

#### Project Outline

Next step is to mount docker as a volume in the server.
Since we are going to build docker images in our pipelines we are going to need to docker commands available in the Jenkins container. 
The way to do that is by mount the docker runtime from the droplet to the Jenkins container as a volume and will make docker commands available in the Jenkins container.

![Image 1](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image1.png?ref_type=heads)


#### Getting started

To proceed we will have to stop the currently running container and create a new one

In the new container we will also utlilise the volume jenkins_home so that we can restore all of our installtions in the previous container aswell as implementing the docker runtime parameters.

Here is how we can run it

```
docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker jenkins/jenkins:lts
```

The provided command executes the docker run command, which is used to create and run a new Docker container. Here's a breakdown of the various components and their functionalities:

•	-p 8080:8080 -p 50000:50000: This maps the host machine's ports 8080 and 50000 to the container's ports 8080 and 50000, respectively. Port 8080 is commonly used for accessing the Jenkins web interface, while port 50000 is used for Jenkins agent communication.

•	-d: This flag runs the container in detached mode, meaning it runs in the background and does not attach to the console. This allows the container to continue running even if the terminal session ends.

•	-v jenkins_home:/var/jenkins_home: This mounts the volume named jenkins_home on the host machine to the /var/jenkins_home directory inside the container. This volume is typically used to persist Jenkins data, including configuration, plugins, and job information.

•	-v /var/run/docker.sock:/var/run/docker.sock: This mounts the Docker socket (docker.sock) on the host machine to the same path inside the container. This allows the container to communicate with the Docker daemon on the host and perform Docker-related operations.

•	-v $(which docker):/usr/bin/docker: This mounts the Docker binary from the host machine to the /usr/bin/docker path inside the container. This enables the container to execute Docker commands, facilitating interactions with the host's Docker daemon.

•	jenkins/jenkins:lts: This specifies the Docker image to be used for creating the container. In this case, it uses the jenkins/jenkins:lts image, which provides a Jenkins installation based on the Long-Term Support (LTS) release.


Overall, this command creates and runs a Docker container with Jenkins, ensuring port mappings, volume mounts for Jenkins data and Docker socket, and access to the Docker binary inside the container.

![Image 2](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image2.png?ref_type=heads)

And can see when accessing everyhting still in place

![Image 3](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image3.png?ref_type=heads)

Can also see docker is available when we go inside the Jenkins container

![Image 4](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image4.png?ref_type=heads)

In order to allow permission for Jenkins to run docker commands, have to give permissions

```
chmod 666 /var/run/docker.sock
```

![Image 5](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image5.png?ref_type=heads)

Can see read/write permissions were added

![Image 6](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image6.png?ref_type=heads)


Can exec into the container 

![Image 7](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image7.png?ref_type=heads)

Allowed to pull the image

![Image 8](https://gitlab.com/FM1995/docker-in-jenkins-2024/-/raw/main/Images/Image8.png?ref_type=heads)

